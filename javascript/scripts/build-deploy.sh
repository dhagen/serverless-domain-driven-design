#!/usr/bin/env bash
set -e -u

export AWS_PROFILE="default"
export AWS_REGION="eu-west-1"
export aws_stage="kb-local-$(whoami)"
export aws_s3_bucket="baasie.cf.sac2019.$aws_stage"

#npm run test
npm run build

if ! aws --profile $AWS_PROFILE --region $AWS_REGION s3api head-bucket --bucket "$aws_s3_bucket" | grep -q "Not Found"; then
  echo "Creating bucket"
  aws s3 mb --profile $AWS_PROFILE s3://$aws_s3_bucket --region $AWS_REGION
else
  echo "Here"
fi
aws cloudformation package --profile $AWS_PROFILE --region $AWS_REGION --template-file template.yaml --s3-bucket ${aws_s3_bucket} --s3-prefix ${aws_stage} --output-template-file packaged.yaml
aws cloudformation deploy --profile $AWS_PROFILE --region $AWS_REGION --template-file ./packaged.yaml --stack-name sac2019-js-${aws_stage} --parameter-overrides Stage=${aws_stage} --capabilities CAPABILITY_IAM

echo API_URL = $(aws --profile $AWS_PROFILE --region $AWS_REGION cloudformation describe-stacks --stack-name sac2019-js-$aws_stage --query 'Stacks[0].Outputs[?OutputKey==`ParkingGarageApi`].OutputValue' --output text)