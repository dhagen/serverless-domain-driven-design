#!/usr/bin/env bash
set -e

export AWS_PROFILE="default"
export AWS_REGION="eu-west-1"
export aws_stage="kb-local-$(whoami)"
export aws_s3_bucket="baasie.cf.sac2019.$aws_stage"

aws cloudformation delete-stack --stack-name sac2019-js-${aws_stage}
aws s3 rb --profile $AWS_PROFILE s3://$aws_s3_bucket --region $AWS_REGION --force
